#include "consoleui.h"

consoleUi::consoleUi() {
    printWelcome();
}

void consoleUi::start() {
    char action;
    do {
        printActions();
        cin >> action;
        execute(action);
    } while (action != '6');                // 6 indicates the quit action
}

void consoleUi::printActions() {
    cout << "Select action" << endl;
    cout << "1: add" << "   " << "2: display" << "   " << "3: connect" << "    " << "4: find" << "   " << "5: remove" << "   " << "6: quit" << endl;
}

void consoleUi::execute(char action) {
    switch (action) {
    case '1':
        add();
        break;
    case '2':
        display();
        break;
    case '3':
        connect();
        break;
    case '4':
        find();
        break;
    case '5':
        remove();
        break;
    case '6':
        cout << "Goodbye..." << endl;
        break;
    default:
        cout << "That is not a valid command, please pick again." << endl;
    }
}

void consoleUi::display() {
    string response;
    cout << "What would you like to display?" << endl;
    cout << "1: Persons" << endl << "2: Computers" << endl;
    cin >>  response;
    if (response == "1") {
        sortPersons();
    }
    else if (response == "2") {
        sortComputers();
    }
    else {
        cout << "Not valid." << endl;
    }
}

void consoleUi::add() {
    string response;
    cout << "What would you like to add?" << endl;
    cout << "1: Person" << endl << "2: Computer" << endl;
    cin >>  response;
    if (response == "1") {
        Person p = fillPersonInfo();
        service.addPerson(p);
    }
    else if (response == "2") {
        Computer c = fillComputerInfo();
        service.addComputer(c);
    }
    else {
        cout << "Not valid." << endl;
    }
}

void consoleUi::sortPersons() {
    string sortingMethod, order;
    cout << endl;
    cout << "Select a sorting method: " << endl;
    cout << "1: By ID" << endl;
    cout << "2: By Name" << endl;
    cout << "3: By year of birth" << endl;
    cout << "4: By year of death" << endl;
    cout << "5: By gender" << endl;
    cin >> sortingMethod;
    while (atoi(sortingMethod.c_str()) < 1 || atoi(sortingMethod.c_str()) > 5) {        // error check
        cout << "Invalid sorting method. Try again." << endl;
        cin >> sortingMethod;
    }
    cout << "Select order: " << endl;
    cout << "1: Ascending" << endl;
    cout << "2: Descending" << endl;
    cin >> order;
    while (atoi(order.c_str()) < 1 || atoi(order.c_str()) > 2) {                        // error check
        cout << "Invalid order. Try again." << endl;
        cin >> order;
    }
    displayPersons(service.sortPersons(sortingMethod, order));

}

void consoleUi::sortComputers() {
    string sortingMethod, order;
    cout << endl;
    cout << "Select a sorting method: " << endl;
    cout << "1: By ID" << endl;
    cout << "2: By name" << endl;
    cout << "3: By year built" << endl;
    cout << "4: By built or not" << endl;
    cout << "5: By type" << endl;
    cin >> sortingMethod;
    while (atoi(sortingMethod.c_str()) < 1 || atoi(sortingMethod.c_str()) > 5) {        // error check
        cout << "Invalid sorting method. Try again." << endl;
        cin >> sortingMethod;
    }
    cout << "Select order: " << endl;
    cout << "1: Ascending" << endl;
    cout << "2: Descending" << endl;
    cin >> order;
    while (atoi(order.c_str()) < 1 || atoi(order.c_str()) > 2) {                        // error check
        cout << "Invalid order. Try again." << endl;
        cin >> order;
    }
    displayComputers(service.sortComputers(sortingMethod, order));
}

void consoleUi::find() {
    string response, searchString;
    cout << "What would you like to search for?" << endl;
    cout << "1: Person" << endl << "2: Computer" << endl;
    cin >>  response;
    if (response == "1") {
        cout << endl << "Enter term to search for: ";
        newline();
        getline(cin,searchString);
        findPerson(searchString);
    }
    else if (response == "2") {
        cout << endl << "Enter term to search for: ";
        newline();
        getline(cin,searchString);
        findComputer(searchString);
    }
    else {
        cout << "Not valid." << endl;                                                   // error message
    }
}

void consoleUi::findPerson(const string& searchString) {
    vector<Person> personVector = service.findPerson(searchString);
    int size = personVector.size();
    if (size == 0) {
        cout << "Person not found." << endl << endl;
    }
    else if (size == 1) {
        cout << "The list contains 1 instance of " << searchString << "." << endl;
        displayPersons(personVector);
        cout << endl;
    }
    else {
        cout << "The list contains " << size << " instances of " << searchString << "." << endl;
        displayPersons(personVector);
        cout << endl;
    }
}

void consoleUi::findComputer(const string& searchString) {
    vector<Computer> computerVector = service.findComputer(searchString);
    int size = computerVector.size();
    if (size == 0) {
        cout << "Computer not found." << endl << endl;
    }
    else if (size == 1) {
        cout << "The list contains 1 instance of " << searchString << "." << endl;
        displayComputers(computerVector);
        cout << endl;
    }
    else {
        cout << "The list contains " << size << " instances of " << searchString << "." << endl;
        displayComputers(computerVector);
        cout << endl;
    }
}

Person consoleUi::fillPersonInfo() {
    string name, birthYear, deathYear, gender, aliveResponse;
    cout << endl;
    newline();
    cout << "Name: ";
    getline(cin,name);
    cout << "Born: ";
    cin >> birthYear;
    cout << "1: Alive" << endl << "2: Dead" << endl;
    cin >> aliveResponse;
    while (!(aliveResponse == "1" || aliveResponse == "2")) {                                           // error check
        cout << "Input not valid. Try again." << endl;
        cin >> aliveResponse;
    }
    if (aliveResponse == "2") {
        cout << "Died: ";
        cin >> deathYear;
        while (atoi(deathYear.c_str()) < atoi(birthYear.c_str())) {                                     // error check
            cout << "Invalid year of death. Try again." << endl;
            cout << "Died: ";
            cin >> deathYear;
        }
    }
    cout << "Gender (male/female): ";
    cin >> gender;
    while (!(gender == "male" || gender == "Male" || gender == "female" || gender == "Female")) {       // error check
        cout << "Invalid gender. Try again." << endl;
        cout << "Gender (male/female): ";
        cin >> gender;
    }
    cout << endl;
    return Person(name, birthYear, deathYear, gender);
}

Computer consoleUi::fillComputerInfo() {
    string name, yearBuilt, built, type;
    bool builtBool = 0;
    cout << endl;
    newline();
    cout << "Name: ";
    getline(cin,name);
    cout << "Was it built (y/n)? ";
    do {
        cin >> built;
        if (built == "y" || built == "Y") {
            builtBool = 1;
        }
        else if (built == "n" || built == "N") {
            builtBool = 0;
        }
        else {
            cout << "Input not valid. Try again." << endl;
        }
    } while (!(built == "y" || built == "Y" || built == "n" || built == "N"));                              // error check
    if (builtBool) {
        cout << "What year?: ";
        cin >> yearBuilt;
    }
    else {
        yearBuilt = " ";
    }
    cout << "Type of computer (e.g. electronic): ";
    newline();
    getline(cin, type);
    cout << endl;
    return Computer(name, yearBuilt, builtBool, type);
}

void consoleUi::displayPersonInfo(const Person& p) {
    cout << left << p.getId() << " " << setw(30) << p.getName() << p.getBirthyear();
    cout << "-" << setw(7) << p.getDeathyear() << setw(7) << p.getGender() << endl;
}

void consoleUi::displayComputerInfo(const Computer& c) {
    cout << left << setw(3) << c.getId() << setw(25) << c.getName();
    if (c.getBuilt() == 0) {
        cout << setw(15) << "Never built";
    }
    else if (c.getBuilt() == 1) {
        cout << "Built " << setw(9) << c.getYearBuilt();
    }
    cout << setw(20) << c.getType();
    cout << endl;
}

void consoleUi::displayPersons(const vector<Person>& personVector) {
    cout << endl;
    cout << setfill('-') << setw(WIDTH) << "-" << endl;
    cout << setfill('-') << setw(WIDTH) << "-" << endl;
    cout << setfill (' ');
    for(unsigned int i = 0; i < personVector.size(); i++) {
        displayPersonInfo(personVector[i]);
        displaySubComputers(service.getConnectedComputers(personVector[i].getId()));
    }
}

void consoleUi::displayComputers(const vector<Computer> &computerVector) {
    cout << endl;
    cout << setfill('-') << setw(WIDTH) << "-" << endl;                                 // a line of dashes
    cout << setfill('-') << setw(WIDTH) << "-" << endl;                                 // a line of dashes
    cout << setfill (' ');                                                              // reset setfill to empty space
    for(unsigned int i = 0; i < computerVector.size(); i++) {
        displayComputerInfo(computerVector[i]);
        displaySubPersons(service.getConnectedPersons(computerVector[i].getId()));
    }
}

void consoleUi::displaySubComputers(const vector<Computer>& computerVector) {
    cout << setfill('.') << setw(WIDTH) << "." << endl;                                 // a line of dots
    cout << setfill (' ');                                                              // reset setfill to empty space
    for(unsigned int i = 0; i < computerVector.size(); i++) {
        cout << "   ";                                                                  // indent
        displayComputerInfo(computerVector[i]);
    }
    cout << setfill('-') << setw(WIDTH) << "-" << endl;                                 // a line of dashes
    cout << setfill('-') << setw(WIDTH) << "-" << endl;                                 // a line of dashes
    cout << setfill (' ');                                                              // reset setfill to empty space
}

void consoleUi::displaySubPersons(const vector<Person>& personVector) {
    cout << setfill('.') << setw(WIDTH) << "." << endl;                                 // a line of dots
    cout << setfill (' ');                                                              // reset setfill to empty space
    for(unsigned int i = 0; i < personVector.size(); i++) {
        cout << "   ";                                                                  // indent
        displayPersonInfo(personVector[i]);
    }
    cout << setfill('-') << setw(WIDTH) << "-" << endl;                                 // a line of dashes
    cout << setfill('-') << setw(WIDTH) << "-" << endl;                                 // a line of dashes
    cout << setfill (' ');                                                              // reset setfill to empty space
}

void consoleUi::connect() {
    string personNumber, computerNumber;
    int personCount, computerCount;
    vector<Computer> computerVector = service.sortComputers("1", "1");          //get computers from the database in alphabetical order
    vector<Person> personVector = service.sortPersons("1", "1");                //get persons from the database in alphabetical order
    personCount = personVector.size();
    computerCount = computerVector.size();
    cout << endl;
    if (personCount == 0) {
        cout << "Error. No persons to connect." << endl;
    }
    else if (computerCount == 0) {
        cout << "Error. No computers to connect to." << endl;
    }
    else {
        for (int i = 0; i < personCount; i++) {
            cout << i+1 << ": " << personVector[i].getName() << endl;
        }
        cout << "Select the person you want to connect a computer to (1-" << personCount << "): ";
        cin >> personNumber;
        while (atoi(personNumber.c_str()) < 1 || atoi(personNumber.c_str()) > personCount) {                        // error check
            cout << "Invalid. Try again." << endl;
            cin >> personNumber;
        }
        cout << endl;
        for (int i = 0; i < computerCount; i++) {
            cout << i+1 << ": " << computerVector[i].getName() << endl;
        }
        cout << "Select the computer you want to connect the person to (1-" << computerCount << "): ";
        cin >> computerNumber;
        while (atoi(computerNumber.c_str()) < 1 || atoi(computerNumber.c_str()) > computerCount) {                   // error check
            cout << "Invalid. Try again." << endl;
            cin >> computerNumber;
        }
        cout << endl;
        if (!service.connect(personVector[atoi(personNumber.c_str())-1].getId(), computerVector[atoi(computerNumber.c_str())-1].getId())) { // -1 to correct the user input
            cout << "Connection has already been made." << endl;
        }
        else {
            cout << "Connection successful." << endl;
        }
    }
}

void consoleUi::remove() {
    string response;
    cout << "What would you like remove?" << endl;
    cout << "1: Person" << endl << "2: Computer" << endl;
    cin >>  response;
    if (response == "1") {
        removePerson();
    }
    else if (response == "2") {
        removeComputer();
    }
    else {
        cout << "Not valid." << endl;
    }
}

void consoleUi::removePerson() {
    string personNumber;
    int size;
    vector<Person> personVector = service.sortPersons("1", "1");            //get persons from the database in alphabetical order
    size = personVector.size();
    if (size == 0) {
        cout << "Error. No person to remove." << endl;
    }
    else {
        for (int i = 0; i < size; i++) {
            cout << i+1 << ": " << personVector[i].getName() << endl;
        }
        cout << "Select the person you want to remove: ";
        cin >> personNumber;
        while (atoi(personNumber.c_str()) < 1 || atoi(personNumber.c_str()) > size) {           // error in input
            cout << "Invalid. Try again." << endl;
            cin >> personNumber;
        }
        service.removePerson(personVector[atoi(personNumber.c_str())-1].getId());               // -1 to correct the user input
    }
}

void consoleUi::removeComputer() {
    string computerNumber;
    int size;
    vector<Computer> computerVector = service.sortComputers("1", "1");       //get computers from the database in alphabetical order
    size = computerVector.size();
    if (size == 0) {
        cout << "Error. No computer to remove." << endl;
    }
    else {
        for (int i = 0; i < size; i++) {
            cout << i+1 << ": " << computerVector[i].getName() << endl;
        }
        cout << "Select the computer you want to remove: ";
        cin >> computerNumber;
        while (atoi(computerNumber.c_str()) < 1 || atoi(computerNumber.c_str()-1) > size) {        // error check
            cout << "Invalid. Try again." << endl;
            cin >> computerNumber;
        }
        service.removeComputer(computerVector[atoi(computerNumber.c_str())-1].getId());            // -1 to correct the user input
    }
}

void consoleUi::printWelcome() {
    ifstream welcome ("../welcome.txt");
    if (welcome.is_open()) {
        cout << welcome.rdbuf();                                                                    // reads from file and sends it to the cout stream
        welcome.close();
    }
    else {
        cout << "Error. Unable to print welcome." << endl;                                          // error check
    }
}

void consoleUi::newline() {
    char input;
    do
    {
        cin.get(input);
    } while (input != '\n');                                                                        // to clear the cin stream
}
