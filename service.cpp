#include "Service.h"

Service::Service() {
}

void Service::addComputer(const Computer& c) {
    computerRepo.add(c);
}

void Service::addPerson(const Person& p) {
    personRepo.add(p);
}

vector<Person> Service::findPerson(const string& searchString) {
    return personRepo.find(searchString);
}

vector<Computer> Service::findComputer(const string& searchString) {
    return computerRepo.find(searchString);
}

vector<Person> Service::sortPersons(const string& sortingMethod, const string& orderNumber) {
    string columnName, order;
    if (sortingMethod == "1") {
        columnName = "ID";
    }
    else if (sortingMethod == "2") {
        columnName = "Name";
    }
    else if (sortingMethod == "3") {
        columnName = "Birthyear";
    }
    else if (sortingMethod == "4") {
        columnName = "Deathyear";
    }
    else if (sortingMethod == "5") {
        columnName = "Gender";
    }
    if (orderNumber == "1") {
        order = "ASC";
    }
    else if (orderNumber == "2") {
        order = "DESC";
    }
    return personRepo.sort(columnName, order);
}

vector<Computer> Service::sortComputers(const string &sortingMethod, const string &orderNumber) {
    string columnName, order;
    if (sortingMethod == "1") {
        columnName = "ID";
    }
    else if (sortingMethod == "2") {
        columnName = "Name";
    }
    else if (sortingMethod == "3") {
        columnName = "Yearbuilt";
    }
    else if (sortingMethod == "4") {
        columnName = "Built";
    }
    else if (sortingMethod == "5") {
        columnName = "Type";
    }
    if (orderNumber == "1") {
        order = "ASC";
    }
    else if (orderNumber == "2") {
        order = "DESC";
    }
    return computerRepo.sort(columnName, order);
}

bool Service::connect(int personId, int computerId) {
    return personRepo.connect(personId, computerId);
}

vector<Computer> Service::getConnectedComputers(int personId) {
    return computerRepo.getConnectedComputers(personId);
}

vector<Person> Service::getConnectedPersons(int computerId) {
    return personRepo.getConnectedPersons(computerId);
}

void Service::removePerson(int personId) {
    personRepo.removePerson(personId);
}

void Service::removeComputer(int computerId) {
    computerRepo.removeComputer(computerId);
}
