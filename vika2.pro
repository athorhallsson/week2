#-------------------------------------------------
#
# Project created by QtCreator 2014-11-27T14:17:23
#
#-------------------------------------------------

QT       += core

QT       -= gui

QT       += core sql

TARGET = vika2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    consoleui.cpp \
    person.cpp \
    personrepository.cpp \
    computer.cpp \
    computerrepository.cpp \
    service.cpp

HEADERS += \
    consoleui.h \
    person.h \
    personrepository.h \
    computer.h \
    computerrepository.h \
    service.h \
    settings.h

OTHER_FILES += \
    welcome.txt
