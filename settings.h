#ifndef SETTINGS_H
#define SETTINGS_H

#include <QtSql>

const int WIDTH = 69;                                           // width of the consoleUI
const QString DBNAME = "../week2.sqlite";                       // name and path of the database
const QString personConnectionName = "PersonConnection";        // name for connection from PersonRepository
const QString computerConnectionName = "ComputerConnection";    // name for connection from ComputerRepository

#endif // SETTINGS_H
