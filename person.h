#ifndef PERSON_H
#define PERSON_H

#include <string>
using namespace std;

class Person {
public:
    Person();
    Person(const string& myName, const string& myBirthYear, const string& myDeathyear, const string& myGender);
    Person(int myId, const string& myName, const string& myBirthYear, const string& myDeathyear, const string& myGender);
    string getName() const;
    string getBirthyear() const;
    string getDeathyear() const;
    string getGender() const;
    int getId() const;
private:
    int id;
    string name;
    string birthYear;
    string deathYear;
    string gender;
};

#endif // PERSON_H
