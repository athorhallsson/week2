#ifndef Service_H
#define Service_H

#include "personrepository.h"
#include "computerrepository.h"

class Service {
public:
    Service();
    // receives a person and sends it to the personRepo so it can be added to the database
    void addPerson(const Person& p);
    // receives a computer and sends it to the computerRepo so it can be added to the database
    void addComputer(const Computer &c);
    // receives a personId and a computerId and sends it to the personRepo so they can be connected, returns true if successful
    bool connect(int personId, int computerId);
    // receives a string to search for and sends it to the personRepo so it can search and return a vector of persons
    // then returns that vector
    vector<Person> findPerson(const string& searchString);
    // receives a string to search for and sends it to the computerRepo so it can search and return a vector of computers
    // then returns that vector
    vector<Computer> findComputer(const string& searchString);
    // receives two strings to specify sortingmethod and order, then translates those strings and sends them to the personRepo
    // the personRepo returns a vector of sorted persons to the service and the service returns that vector
    vector<Person> sortPersons(const string& sortingMethod, const string& orderNumber);
    // receives two strings to specify sortingmethod and order, then translates those strings and sends them to the computerRepo
    // the computerRepo returns a vector of sorted computers to the service and the service returns that vector
    vector<Computer> sortComputers(const string& sortingMethod, const string& orderNumber);
    // receives a computerId and sends it to the personRepo, then the personRepo returns a vector of persons that are connected to
    // that computerId. The service then returns that vector.
    vector<Person> getConnectedPersons(int computerId);
    // receives a personId and sends it to the computerRepo, then the computerRepo returns a vector of computers that are connected to
    // that personId. The service then returns that vector.
    vector<Computer> getConnectedComputers(int personId);
    // receives a personId and tells the personRepo to remove the person with that ID
    void removePerson(int personId);
    // receives a computerId and tells the computerRepo to remove the computer with that ID
    void removeComputer(int computerId);
private:
    PersonRepository personRepo;
    ComputerRepository computerRepo;
};

#endif // Service_H
