#include "personrepository.h"

PersonRepository::PersonRepository() {
    open();
}

PersonRepository::~PersonRepository() {
    if (QSqlDatabase::contains(personConnectionName)) {
        db.close();
        db.removeDatabase(DBNAME);
    }
}

void PersonRepository::open() {
    if (QSqlDatabase::contains(personConnectionName)) {
        db = QSqlDatabase::database(personConnectionName);
    }
    else {
        db = QSqlDatabase::addDatabase("QSQLITE", personConnectionName);
        db.setDatabaseName(DBNAME);
        db.open();
        QSqlQuery query(db);
        query.exec(QString("Pragma foreign_keys = on"));        // turn on foreign_key check in database
    }
    if (db.tables().count() == 0) {                             // if the database has no tables, create them
        createDatabase();
    }
}

void PersonRepository::add(const Person& p) {
    QSqlQuery query(db);
    query.exec(QString("INSERT INTO Persons (Name, Birthyear, Deathyear, Gender) VALUES ('%1','%2','%3','%4')")
    .arg(QString::fromStdString(p.getName()))
    .arg(QString::fromStdString(p.getBirthyear()))
    .arg(QString::fromStdString(p.getDeathyear()))
    .arg(QString::fromStdString(p.getGender())));
}

vector<Person> PersonRepository::sort(const string& columnName, const string& order) {
    QSqlQuery query(db);
    query.exec(QString("SELECT ID, Name, Birthyear, Deathyear, Gender FROM Persons p WHERE Deleted = 0 ORDER BY p.%1 %2")
    .arg(QString::fromStdString(columnName))
    .arg(QString::fromStdString(order)));
    return buildVector(query);
}

vector<Person> PersonRepository::buildVector(QSqlQuery& query) {
    vector<Person> personVector;
    while (query.next()) {
        int id = query.value("ID").toInt();
        string name = query.value("Name").toString().toStdString();
        string birthyear = query.value("Birthyear").toString().toStdString();
        string deathyear = query.value("Deathyear").toString().toStdString();
        string gender = query.value("Gender").toString().toStdString();
        Person p = Person(id, name, birthyear, deathyear, gender);
        personVector.push_back(p);
    }
    return personVector;
}

vector<Person> PersonRepository::find(const string& searchString) {
    QSqlQuery query(db);
    query.exec(QString("SELECT ID, Name, Birthyear, Deathyear, Gender FROM Persons WHERE Deleted = 0 AND (Name LIKE '%%1%' OR Birthyear LIKE '%%1%' OR Deathyear LIKE '%%1%' OR Gender LIKE '%%1%')")
    .arg(QString::fromStdString(searchString)));
    return buildVector(query);
}

bool PersonRepository::connect(int personId, int computerId) {
    QSqlQuery query(db);
    query.exec(QString("SELECT p_ID, c_ID FROM Owners WHERE p_ID = %1 AND c_ID = %2")      // check if the entry already exists
    .arg(personId)
    .arg(computerId));
    vector<int> idVector;
    while (query.next()) {
        int id = query.value("p_ID").toInt();
        idVector.push_back(id);
    }
    if (idVector.size() == 0) {
        query.exec(QString("INSERT INTO Owners (p_ID, c_ID) VALUES (%1, %2)")
        .arg(personId)
        .arg(computerId));
        return true;
    }
    return false;
}

vector<Person> PersonRepository::getConnectedPersons(int computerId) {
    QSqlQuery query(db);
    query.exec(QString("SELECT ID, Name, Birthyear, Deathyear, Gender FROM Persons JOIN Owners ON Owners.p_ID = Persons.ID WHERE Owners.c_ID = %1 AND Deleted = 0 ORDER BY Persons.Name")
    .arg(computerId));
    return buildVector(query);
}

void PersonRepository::removePerson(int personId) {
    QSqlQuery query(db);
    query.exec(QString("UPDATE Persons SET Deleted = 1 WHERE ID = %1")
    .arg(personId));
}

void PersonRepository::createDatabase() {
    QSqlQuery query(db);
    query.exec("CREATE TABLE 'Computers' ('ID' INTEGER PRIMARY KEY  NOT NULL , 'Name' VARCHAR NOT NULL , 'YearBuilt' VARCHAR, 'Type' VARCHAR, 'Built' BOOL, 'Deleted' BOOL NOT NULL  DEFAULT 0)");
    query.exec("CREATE TABLE 'Persons' ('ID' INTEGER PRIMARY KEY  NOT NULL , 'Name' VARCHAR NOT NULL , 'Birthyear' VARCHAR, 'Deathyear' VARCHAR, 'Gender' VARCHAR, 'Deleted' BOOL NOT NULL  DEFAULT 0)");
    query.exec("CREATE TABLE 'Owners' ('c_ID' INTEGER, 'p_ID' INTEGER, FOREIGN KEY (c_ID) REFERENCES Computers(ID), FOREIGN KEY (p_ID) REFERENCES Persons(ID), PRIMARY KEY (c_ID, p_ID))");
}
