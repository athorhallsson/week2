#ifndef COMPUTER_H
#define COMPUTER_H

#include <string>
using namespace std;

class Computer {
public:
    Computer ();
    Computer (const string& myName, const string& myYearBuilt, bool myBuilt, const string& myType);
    Computer (int myId, const string& myName, const string& myYearBuilt, bool myBuilt, const string& myType);
    string getName() const;
    string getYearBuilt() const;
    bool getBuilt() const;
    string getType() const;
    int getId() const;
private:
    int id;
    string name;
    string yearBuilt;
    bool built;
    string type;
};

#endif // COMPUTER_H
