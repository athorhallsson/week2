#include "person.h"

Person::Person() {
    id = 0;
    name = "";
    birthYear = "";
    deathYear = "";
    gender = "";
}

Person::Person(const string& myName, const string& myBirthYear, const string& myDeathyear, const string& myGender) {
    id = 0;
    name = myName;
    birthYear = myBirthYear;
    deathYear = myDeathyear;
    gender = myGender;
}

Person:: Person(int myId, const string& myName, const string& myBirthYear, const string& myDeathyear, const string& myGender) {
    id = myId;
    name = myName;
    birthYear = myBirthYear;
    deathYear = myDeathyear;
    gender = myGender;
}

string Person::getName() const {
    return name;
}

string Person::getBirthyear() const {
    return birthYear;
}

string Person::getDeathyear() const {
    return deathYear;
}

string Person::getGender() const {
    return gender;
}

int Person::getId() const {
    return id;
}
