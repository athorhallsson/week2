#include "computerrepository.h"

ComputerRepository::ComputerRepository() {
    open();
}

ComputerRepository::~ComputerRepository() {
    if (QSqlDatabase::contains(computerConnectionName)) {
        db.close();
        db.removeDatabase(DBNAME);
    }
}

void ComputerRepository::open() {
    if (QSqlDatabase::contains(computerConnectionName)) {
        db = QSqlDatabase::database(computerConnectionName);
    }
    else {
        db = QSqlDatabase::addDatabase("QSQLITE", computerConnectionName);
        db.setDatabaseName(DBNAME);
        db.open();
        QSqlQuery query(db);
        query.exec(QString("Pragma foreign_keys = on"));        // turn on foreign_key check in database
    }
    if (db.tables().count() == 0) {                             // if the database has no tables, create them
        createDatabase();
    }
}

void ComputerRepository::add(const Computer& c) {
    QSqlQuery query(db);
    query.exec(QString("INSERT INTO Computers (Name, Yearbuilt, Built, Type) VALUES ('%1','%2','%3','%4')")
    .arg(QString::fromStdString(c.getName()))
    .arg(QString::fromStdString(c.getYearBuilt()))
    .arg(c.getBuilt())
    .arg(QString::fromStdString(c.getType())));
}

vector<Computer> ComputerRepository::sort(const string& columnName, const string& order) {
    QSqlQuery query(db);
    query.exec(QString("SELECT ID, Name, YearBuilt, Built, Type FROM Computers c WHERE Deleted = 0 ORDER BY c.%1 %2")
    .arg(QString::fromStdString(columnName))
    .arg(QString::fromStdString(order)));
    return buildVector(query);
}

vector<Computer> ComputerRepository::getConnectedComputers(int personId) {
    QSqlQuery query(db);
    query.exec(QString("SELECT ID, Name, YearBuilt, Built, Type FROM Computers JOIN Owners ON Owners.c_ID = computers.ID WHERE Owners.p_ID =%1 AND Computers.Deleted = 0 ORDER BY Computers.Name")
    .arg(personId));
    return buildVector(query);
}

vector<Computer> ComputerRepository::buildVector(QSqlQuery& query) {
    vector<Computer> computerVector;
    while (query.next()) {
        int id = query.value("ID").toInt();
        string name = query.value("Name").toString().toStdString();
        string yearBuilt = query.value("Yearbuilt").toString().toStdString();
        bool built = query.value("Built").toBool();
        string type = query.value("Type").toString().toStdString();
        Computer c = Computer(id, name, yearBuilt, built, type);
        computerVector.push_back(c);
    }
    return computerVector;
}

vector<Computer> ComputerRepository::find(const string& searchString) {
    QSqlQuery query(db);
    query.exec(QString("SELECT ID, Name, YearBuilt, Built, Type FROM Computers WHERE Deleted = 0 AND (Name LIKE '%%1%' OR Yearbuilt LIKE '%%1%' OR Built LIKE '%%1%' OR Type LIKE '%%1%')")
    .arg(QString::fromStdString(searchString)));
    return buildVector(query);
}

void ComputerRepository::removeComputer(int computerId) {
    QSqlQuery query(db);
    query.exec(QString("UPDATE Computers SET Deleted = 1 WHERE ID = %1")
    .arg(computerId));
}

void ComputerRepository::createDatabase() {
    QSqlQuery query(db);
    query.exec("CREATE TABLE 'Computers' ('ID' INTEGER PRIMARY KEY  NOT NULL , 'Name' VARCHAR NOT NULL , 'YearBuilt' VARCHAR, 'Type' VARCHAR, 'Built' BOOL, 'Deleted' BOOL NOT NULL  DEFAULT 0)");
    query.exec("CREATE TABLE 'Persons' ('ID' INTEGER PRIMARY KEY  NOT NULL , 'Name' VARCHAR NOT NULL , 'Birthyear' VARCHAR, 'Deathyear' VARCHAR, 'Gender' VARCHAR, 'Deleted' BOOL NOT NULL  DEFAULT 0)");
    query.exec("CREATE TABLE 'Owners' ('c_ID' INTEGER, 'p_ID' INTEGER, FOREIGN KEY (c_ID) REFERENCES Computers(ID), FOREIGN KEY (p_ID) REFERENCES Persons(ID), PRIMARY KEY (c_ID, p_ID))");
}
