#ifndef COMPUTERREPOSITORY_H
#define COMPUTERREPOSITORY_H

#include "computer.h"
#include "settings.h"
#include <vector>
#include <QtSql>

class ComputerRepository {
public:
    ComputerRepository();
    ~ComputerRepository();
    // adds a computer to the database
    void add(const Computer& c);
    // returns a vector with computers in a specific order by columnName
    vector<Computer> sort(const string& columnName, const string& order);
    // returns a vector with computers connected to the personId
    vector<Computer> getConnectedComputers(int personId);
    // returns a vector with all computers which include searchString in their attributes
    vector<Computer> find(const string& searchString);
    // marks the Deleted column in the database with true (1) where the ID is computerID
    void removeComputer(int computerId);
    // creates the desired tables in the database
    void createDatabase();
private:
    QSqlDatabase db;
    // opens the database
    void open();
    // builds a vector from the query and returns it
    vector<Computer> buildVector(QSqlQuery& query);
};

#endif // COMPUTERREPOSITORY_H
