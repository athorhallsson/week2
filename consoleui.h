#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include <fstream>
#include <iomanip>
#include <iostream>
#include "person.h"
#include "computer.h"
#include "service.h"

class consoleUi {
public:
    consoleUi();
    // starts a simple loop which contains the entire program
    void start();

private:
    Service service;
    // displays a dialog to let the user input information about the person
    Person fillPersonInfo();
    // displays a dialog to let the user input information about the computer
    Computer fillComputerInfo();
    // displays possible actions for the user to choose from
    void printActions();
    // switch statement for all the actions
    void execute(char action);
    // allows the user choose to add a computer or a person
    void add();
    // allows the user to choose to display computers or persons
    void display();
    // allows the user to select in which order to display the list of persons
    void sortPersons();
    // allows the user to select in which order to display the list of computers
    void sortComputers();
    // allows the user to choose to search for persons or computers
    void find();
    // sends a searchString to the service to search for computers
    void findComputer(const string& searchString);
    // sends a searchString to the service to search for persons
    void findPerson(const string& searchString);
    // displays all persons in the personVector
    void displayPersons(const vector<Person>& personVector);
    // displays a person
    void displayPersonInfo(const Person& p);
    // displays a all the persons in the personVector with a larger indent
    void displaySubPersons(const vector<Person>& personVector);
    // displays all computers in the computerVector
    void displayComputers(const vector<Computer>& computerVector);
    // displays a computer
    void displayComputerInfo(const Computer& c);
    // displays a all the computers in the computerVector with a larger indent
    void displaySubComputers(const vector<Computer>& computerVector);
    // allows the user to connect a person to a computer
    void connect();
    // displays the welcome text in the beginning of the program
    void printWelcome();
    // used before getline to clear the cin stream to the next newline character
    void newline();
    // allows the user to choose to remove a person or a computer
    void remove();
    // displays all the persons in the database in alphabetical order and allows the user to pick who to remove
    void removePerson();
    // displays all the computers in the database in alphabetical order and allows the user to pick which one to remove
    void removeComputer();
};

#endif // CONSOLEUI_H
