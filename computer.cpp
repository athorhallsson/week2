#include "computer.h"

Computer::Computer() {
    id = 0;
    name = "";
    yearBuilt = "";
    built = 1;
    type = "";
}

Computer::Computer(const string& myName, const string& myYearBuilt, bool myBuilt, const string& myType) {
    id = 0;
    name = myName;
    yearBuilt = myYearBuilt;
    built = myBuilt;
    type = myType;
}

Computer::Computer (int myId, const string& myName, const string& myYearBuilt, bool myBuilt, const string& myType) {
    id = myId;
    name = myName;
    yearBuilt = myYearBuilt;
    built = myBuilt;
    type = myType;
}

int Computer::getId() const {
    return id;
}

string Computer::getName() const {
    return name;
}

string Computer::getYearBuilt() const {
    return yearBuilt;
}

bool Computer::getBuilt() const {
    return built;
}

string Computer::getType() const {
    return type;
}
